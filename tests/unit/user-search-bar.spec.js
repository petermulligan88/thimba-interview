import { mount, createLocalVue } from '@vue/test-utils';
import VueI18n from 'vue-i18n';
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome';
import { library } from '@fortawesome/fontawesome-svg-core';
import {
  faArrowAltCircleDown, faArrowAltCircleLeft, faUndoAlt, faLanguage,
}
  from '@fortawesome/free-solid-svg-icons';
import UserSearchBar from '../../src/components/UserSearchBar.vue';
import en from '../../src/locales/en';
import mockUsers from '../data/mockUsers';

library.add(faArrowAltCircleDown, faArrowAltCircleLeft, faUndoAlt, faLanguage);


const localVue = createLocalVue();
localVue.use(VueI18n);
localVue.component('font-awesome-icon', FontAwesomeIcon);

// Create VueI18n instance with options
const i18n = new VueI18n({
  locale: 'en',
  fallbackLocale: 'en',
  messages: { en },
});

describe('UserSearchBar.vue', () => {
  let wrapper;

  beforeEach(() => {
    wrapper = mount(UserSearchBar, {
      localVue,
      i18n,
      propsData: {
        filteredUsers: mockUsers,
      },
      computed: {
        roles() {
          return ['user', 'admin', 'guest'];
        },
        firstLettersInNames() {
          return ['a', 'b', 'c'];
        },
      },
    });
  });

  afterEach(() => {
    jest.clearAllMocks();
  });

  it('should change role value when clicked', () => {
    // Find select and check it is set to deault '' value.
    let select = wrapper.find('[data-test="user-role-select"]');
    expect(select.element.value).toBe('');
    // Find and click option 2.
    const option = wrapper.findAll('[data-test="user-role-option"]').at(2);
    option.trigger('click');
    select.trigger('change');
    // Find select and check it is set to deault '' value.
    select = wrapper.find('[data-test="user-role-select"]');
    expect(select.element.value).toBe('');
  });

  it('should change name value when clicked', () => {
    // Find select and check it is set to deault '' value.
    let select = wrapper.find('[data-test="user-name-select"]');
    expect(select.element.value).toBe('');
    // Find and click option 2.
    const option = wrapper.findAll('[data-test="user-name-option"]').at(2);
    option.trigger('click');
    select.trigger('change');
    // Find select and check it is set to deault '' value.
    select = wrapper.find('[data-test="user-name-select"]');
    expect(select.element.value).toBe('');
  });
});
