import { shallowMount, createLocalVue } from '@vue/test-utils';
import Vuex from 'vuex';
import VueI18n from 'vue-i18n';
import Users from '../../src/views/Users.vue';
import mockData from '../data/mockUsers';
import en from '../../src/locales/en';

const localVue = createLocalVue();
localVue.use(Vuex);
localVue.use(VueI18n);

// Create VueI18n instance with options
const i18n = new VueI18n({
  locale: 'en',
  fallbackLocale: 'en',
  messages: { en },
});

describe('Users.vue', () => {
  // eslint-disable-next-line no-unused-vars
  let data;
  let actions;
  let store;
  let wrapper;
  let getters;

  beforeEach(() => {
    data = {
      filteredUsers: mockData,
    };
    getters = {
      users: () => mockData,
    };
    actions = {
      fetchUsers: jest.fn(),
    };
    store = new Vuex.Store({
      getters,
      actions,
    });
  });

  afterEach(() => {
    jest.clearAllMocks();
  });

  it('should dispatch `fetchUsers` when it is created', () => {
    const createdHook = jest.spyOn(Users, 'created');
    wrapper = shallowMount(Users, { store, localVue, i18n });
    expect(createdHook).toHaveBeenCalled();
    expect(actions.fetchUsers).toHaveBeenCalled();
  });

  it('should render the same amount of users as are stored in data', () => {
    wrapper = shallowMount(Users, { store, localVue, i18n });
    expect(wrapper.findAll('[data-test="user"]').length)
      .toEqual(wrapper.vm.filteredUsers.length);
  });
});
