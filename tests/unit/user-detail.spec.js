import { mount, createLocalVue, RouterLinkStub } from '@vue/test-utils';
import Vuex from 'vuex';
import Axios from 'axios';
import VueAxios from 'vue-axios';
import VueI18n from 'vue-i18n';
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome';
import { library } from '@fortawesome/fontawesome-svg-core';
import {
  faArrowAltCircleLeft, faLanguage,
}
  from '@fortawesome/free-solid-svg-icons';
import UserDetail from '../../src/views/UserDetail.vue';
import mockData from '../data/mockUsers';
import en from '../../src/locales/en';

library.add(faArrowAltCircleLeft, faLanguage);


const localVue = createLocalVue();
localVue.use(Vuex);
localVue.use(VueAxios, Axios);
localVue.use(VueI18n);
localVue.component('font-awesome-icon', FontAwesomeIcon);

// Create VueI18n instance with options
const i18n = new VueI18n({
  locale: 'en',
  fallbackLocale: 'en',
  messages: { en },
});

describe('Users.vue', () => {
  let store;
  let wrapper;
  let getters;
  let state;

  beforeEach(() => {
    state = {
      users: mockData,
    };
    getters = {
      getUserById: () => (id) => {
        if (state.users.length) {
          return state.users.find(user => user.id === id);
        }
        return null;
      },
    };
    store = new Vuex.Store({
      getters,
    });
  });

  afterEach(() => {
    jest.clearAllMocks();
  });

  it('should pass `userId` prop to `getUserById` getter and render correct user data', () => {
    wrapper = mount(UserDetail, {
      store,
      localVue,
      stubs: { RouterLink: RouterLinkStub },
      propsData: { userId: '1' },
      i18n,
    });

    expect(wrapper.find('[data-test="id"]').text())
      .toEqual(wrapper.props().userId);

    expect(wrapper.find('[data-test="avatar_url"]').text())
      .toEqual(wrapper.vm.user.avatar);

    expect(wrapper.find('[data-test="name"]').text())
      .toEqual(wrapper.vm.$store.getters.getUserById(wrapper.props().userId).name);

    expect(wrapper.find('[data-test="jobDescription"]').text())
      .toEqual(wrapper.vm.$store.getters.getUserById(wrapper.props().userId).jobDescription);

    expect(wrapper.find('[data-test="userEmail"]').text())
      .toEqual(wrapper.vm.$store.getters.getUserById(wrapper.props().userId).userEmail);

    expect(wrapper.find('[data-test="createdAt"]').text())
      .toEqual(wrapper.vm.$store.getters.getUserById(wrapper.props().userId).createdAt);

    expect(wrapper.find('[data-test="userIpAddress"]').text())
      .toEqual(wrapper.vm.$store.getters.getUserById(wrapper.props().userId).userIpAddress);

    expect(wrapper.find('[data-test="userAgent"]').text())
      .toEqual(wrapper.vm.$store.getters.getUserById(wrapper.props().userId).userAgent);
  });
});
