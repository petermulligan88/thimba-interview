export default [
  {
    id: '1',
    avatar: 'https://www.mylovelyavatar.com/1.png',
    name: 'Mr. Man',
    jobDescription: 'Ninja',
    userEmail: 'Mr@Man.com',
    createdAt: new Date().toDateString(),
    userIpAddress: '127.0.0.1',
    userAgent: 'Mozilla/5.0',
  },
  {
    id: '2',
    avatar: 'https://www.mylovelyavatar.com/1.png',
    name: 'Mrs. Woman',
    jobDescription: 'Pirate',
    userEmail: 'Mrs@Woman.com',
    createdAt: new Date().toDateString(),
    userIpAddress: '127.0.0.2',
    userAgent: 'Mozilla/5.0',
  },
];
