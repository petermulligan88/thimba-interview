import Vue from 'vue';
import Axios from 'axios';
import VueAxios from 'vue-axios';
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome';
import VueI18n from 'vue-i18n';
import en from './locales/en';
import es from './locales/es';
import App from './App.vue';
import router from './router';
import store from './store';
import 'bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css';

require('./helpers/fa-loader');

Vue.component('font-awesome-icon', FontAwesomeIcon);

const axios = Axios.create({
  baseURL: 'http://5dd559d3ce4c300014402cb8.mockapi.io/',
  timeout: 1000,
});

Vue.use(VueAxios, axios);
Vue.use(VueI18n);

Vue.config.productionTip = false;

// Create VueI18n instance with options
const i18n = new VueI18n({
  locale: 'en',
  fallbackLocale: 'en',
  messages: { en, es },
});

new Vue({
  router,
  store,
  i18n,
  render: h => h(App),
}).$mount('#app');
