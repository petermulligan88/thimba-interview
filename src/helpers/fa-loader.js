import { library } from '@fortawesome/fontawesome-svg-core';
import {
  faEye, faArrowAltCircleDown, faArrowAltCircleLeft, faUndoAlt, faLanguage,
}
  from '@fortawesome/free-solid-svg-icons';

library.add(faEye, faArrowAltCircleDown, faArrowAltCircleLeft, faUndoAlt, faLanguage);
