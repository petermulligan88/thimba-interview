import Vue from 'vue';
import Vuex from 'vuex';
import VuexPersistence from 'vuex-persist';
import SecureLS from 'secure-ls';


Vue.use(Vuex);

const ls = new SecureLS({ isCompression: false });
const persist = new VuexPersistence({
  storage: {
    getItem: key => ls.get(key),
    setItem: (key, value) => ls.set(key, value),
    removeItem: key => ls.remove(key),
  },
}).plugin;

export default new Vuex.Store({
  plugins: [persist],
  state: {
    users: {},
  },
  mutations: {
    SAVE_USERS: (state, users) => {
      state.users = users;
    },
  },
  actions: {
    async fetchUsers(context) {
      await Vue.axios.get('/onboard/posts/')
        .then(({ data }) => {
          context.commit('SAVE_USERS', data);
        })
        .catch((error) => {
          // TODO: Handle error.
          console.error(error);
        });
    },
  },
  getters: {
    users: state => state.users,
    getUserById: state => (id) => {
      if (state.users.length) {
        return state.users.find(user => user.id === id);
      }
      return null;
    },
  },
});
