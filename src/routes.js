import store from './store';

export default [
  {
    path: '/',
    name: 'users',
    component: () => import(/* webpackChunkName: "users" */ './views/Users.vue'),
  },
  {
    path: '/:userId',
    name: 'user',
    props: true,
    component: () => import(/* webpackChunkName: "users" */ './views/UserDetail.vue'),
    beforeEnter: (to, from, next) => {
      if (!store.getters.getUserById(to.params.userId)) {
        next({ name: 'users' });
      } else {
        next();
      }
    },
  },
];
