const CompressionPlugin = require('compression-webpack-plugin');

module.exports = {
  lintOnSave: process.env.NODE_ENV !== 'production',

  productionSourceMap: false,

  crossorigin: 'anonymous',

  configureWebpack: {

    devServer: {
      overlay: {
        warnings: true,
        errors: true,
      },
    },

    plugins: [
      /* Compresses output with gzip. */
      new CompressionPlugin({
        filename: '[path].gz[query]',
        algorithm: 'gzip',
        test: /\.js$|\.css$|\.html$|\.eot?.+$|\.ttf?.+$|\.woff?.+$|\.svg?.+$/,
        threshold: 10240,
        minRatio: 0.8,
      }),
    ],
  },
};
