# Thimba Interview Project

A project for the interview process of Thimba Media.


## Dependencies

This project was created with [VueCLI](https://cli.vuejs.org/).


[Axios](https://github.com/axios/axios) is used for HTTP/Ajax requests.


[VueI18n](https://github.com/kazupon/vue-i18n) is used for internationalizing strings.


[Jest](https://jestjs.io/) is used for unit testing.


[Bootstrap](https://getbootstrap.com/)'s CSS is used for layout styling.


[FontAwesome](https://fontawesome.com/) is used for icons.


[vuex-persist](https://www.npmjs.com/package/vuex-persist) and
[secure-ls](https://www.npmjs.com/package/secure-ls) are used to encrypt and persist data in vuex.


It is bundled with [Webpack](https://webpack.js.org/) and transpiled with [Babel](https://babeljs.io/).




## Table of Content
  
 - [Project Setup](https://gitlab.com/petermulligan88/thimba-interview#project-setup)
    - [Clone](https://gitlab.com/petermulligan88/thimba-interview#clone-and-setup-repository)
    - [Run Dev](https://gitlab.com/petermulligan88/thimba-interview#compile-run-and-provide-hot-reloading-for-development-runs-locally-on-port-8080)
    - [Build](https://gitlab.com/petermulligan88/thimba-interview#compile-and-minifies-for-production-output-to-dist)
    - [Unit Test](https://gitlab.com/petermulligan88/thimba-interview#run-your-unit-tests-output-to-stdout)
    - [Lint](https://gitlab.com/petermulligan88/thimba-interview#lints-code-to-match-airbnb-standard)  
---


 - [Short Questions](https://gitlab.com/petermulligan88/thimba-interview#short-questions)
    - [Question 1](https://gitlab.com/petermulligan88/thimba-interview#in-php-how-would-you-get-query-string-parameters)
    - [Question 2](https://gitlab.com/petermulligan88/thimba-interview#in-php-how-do-you-define-a-constant)
    - [Question 3](https://gitlab.com/petermulligan88/thimba-interview#in-javascript-a-string-using-double-quotes-is-exatly-the-same-as-a-string-using-single-quotes)
    - [Question 4](https://gitlab.com/petermulligan88/thimba-interview#what-steps-would-you-take-to-optimize-a-website-to-improve-load-speed)
    - [Question 5](https://gitlab.com/petermulligan88/thimba-interview#in-words-psuedo-code-or-using-words-of-your-choice-explain-how-you-would-write-a-function-that-writes-away-user-name-and-email-to-database)
    - [Question 6](https://gitlab.com/petermulligan88/thimba-interview#why-would-you-use-instead-of-)
    - [Question 7](https://gitlab.com/petermulligan88/thimba-interview#explain-the-difference-between-1st-and-3rd-party-cookies)  
---


 - [Errors](https://gitlab.com/petermulligan88/thimba-interview#find-errors)
    - [Find Errors](https://gitlab.com/petermulligan88/thimba-interview#find-errors-in-the-code-extract-below)
    - [Fixed Behaviour](https://gitlab.com/petermulligan88/thimba-interview#once-established-and-fixed-the-errors-in-the-above-code-choose-how-many-times-above-forloop-would-execute)


## Project Setup
  

### Clone and setup repository.
```
git clone https://gitlab.com/petermulligan88/thimba-interview.git
```
```
cd thimba-interview
```
```
npm install
```
  

### Compile, run, and provide hot-reloading for development. (Runs locally on port 8080)
```
npm run serve
```
  

### Compile and minifies for production. (Output to 'dist')
```
npm run build
```
  

### Run your unit tests (Output to stdout)
```
npm run test:unit
```
  

### Lints code to match AirBnB Standard
```
npm run lint
```
  
  

## Short Questions
  

### In PHP how would you GET query string parameters?
```
// You GET the query string parameter using the $_GET function.
$queryString = $_GET["param"];
// It is usually used with a sanitizer function.
$queryString = htmlspecialchars($_GET["param"]);
```
  

### In PHP, how do you define a constant.

```
define("CONST_NAME", "constValue");
const CONST_NAME = "constValue";    // Must be defined in a top-level element.
```
  

### In Javascript, a string using double quotes is exatly the same as a string using single quotes?
### (True/False)
  
Both?
  
They are mostly the same, but double quoted strings can use single quotes unescaped, and vice versa.
A lot of the popular linters will squawk at you if you use single quotes.
JSON spec uses double quotes.
  
  
### What steps would you take to optimize a website to improve load speed?
  
I would use a bundle profiling tool to find out where the application was bloated, and optimize 
based on that as necessary.
I would ensure best practices for images and fonts such as lazy loading or offsite storage were
being used.
I would use a CSS tree shaking tool.
I would ensure gzip was being used.
I would implement code splitting if necessary.
I would ensure the bundling tool itself wasn't causing issue.
  
  
### In words, psuedo code, or using words of your choice, explain how you would write a function  that writes away user name and email to database.
  
In the frontend, I would use a POST request form submission or ajax call.
  
In the backend, I would pull the parameters from the request and sanitize them. Then I would
validate the parameters. Once they were validated and sanitized, I would attempt to create a 
connection to the database. On success, I would prepare a statement, and attempt the query.
If the database connection or the query failed, I would log the error (Or follow specs).
  

### Why would you use === instead of ==.
  
I would use === when I need to check equality by value and type. If I don't care about the type,
I can use ==. This is true in both PHP and JS.
  

### Explain the difference between 1st and 3rd party cookies.
  
1st party cookies are created and used by the website you are on. 3rd party cookies are used by a
another party, though not necessarily created by one.
This is important in European countries due to the GDPR. 1st party functinal cookies do not require
a website to have a GDPR banner. A website that makes use of 3rd party cookies requires the banner. 
  

  
## Find errors
  

### Find errors in the code extract below
```
<?php
for($i=0;$i<5;$i++) {
  $i=$i++1;
}
echo "$i";
>
```


`$i=$i++1;` is invalid systax, but it is difficult to tell what the logic error is without knowing the
original intent was. `$i=$i+1`, `$i=$i+=1`, and `$i+=1` all "fix" it, but I don't know which was
intended. In this particular case, it doesn't matter, as the result is the same for all 3.

  
### Once established and fixed the errors in the above code, choose how many times above forloop would execute.
### (5, 6, 7, 8)
  
None of the above.
  
The output would be 6, but it would only execute 3 times due to reassigning to `$i`.


---
  
